$('#textarea1').val();
M.textareaAutoResize($('#textarea1'));

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, options);
});

// Or with jQuery

$(document).ready(function() {
    $('.fixed-action-btn').floatingActionButton();
});

//  MOSTRAR VENTANA AGREGAR AMIGO   //

$('#agregarAmigo').click(function(){
    mostrarAgregarAmigo();
});

$('#eliminarAmigo').click(function(){
    mostrarEliminarAmigo();
});

function cerrarAgregarAmigo() {
    document.getElementById('ventanaAgregarAmigo').style.display = 'none';
}

function cerrarEliminarAmigo() {
    document.getElementById('ventanaEliminarAmigo').style.display = 'none';
}

function mostrarAgregarAmigo() {
    document.getElementById('ventanaAgregarAmigo').style.display = 'block';
}

function mostrarEliminarAmigo() {
    document.getElementById('ventanaEliminarAmigo').style.display = 'block';
}

$("#cancelarAgregar").click(function(){
    cerrarAgregarAmigo();
});

$("#cancelarEliminar").click(function(){
    cerrarEliminarAmigo();
});