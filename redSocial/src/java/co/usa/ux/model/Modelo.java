/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usa.ux.model;

import co.usa.ux.controller.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jofre Gavidia
 */
public class Modelo {
    
    ArrayList<Usuario> usuarios;
    ArrayList<Usuario> amigos = new ArrayList();
    //FileWriter fichero=null;
    FileOutputStream fichero;
    
    public void escribirFile() throws IOException, FileNotFoundException, ClassNotFoundException{
        
        File x=new File("C:\\Users\\Jofre Gavidia\\Desktop\\Quinto Semestre\\Diseño de Experencia del Usuario\\Tercer Corte\\Red\\redsocial\\redSocial\\Registros.txt");
        if(!x.exists()){
            try {
                x.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        try {
            fichero= new FileOutputStream(x,false);
            ObjectOutputStream canal = new ObjectOutputStream(fichero);
            canal.writeObject(amigos);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }catch (IOException ex) {
            ex.printStackTrace();
        }finally{
            
            try {
                fichero.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        amigos=null;
    }
    
    public ArrayList<Usuario> leerFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        ArrayList<Usuario> datos = new ArrayList<>();
        try {
            ObjectInputStream entrada = new ObjectInputStream(new FileInputStream("C:\\Users\\Jofre Gavidia\\Desktop\\Quinto Semestre\\Diseño de Experencia del Usuario\\Tercer Corte\\Red\\redsocial\\redSocial\\Registros.txt"));
            datos = (ArrayList) entrada.readObject();
            entrada.close();
        } catch (FileNotFoundException e) {
            System.out.println("No se ha encontrao");
        }
        return datos;
    }
    
    public void convertirUsuario(String x) throws IOException, FileNotFoundException, ClassNotFoundException{
        amigos = leerFile();
        System.out.println(x);
        Usuario user = (Usuario)Utils.fromJson(x, Usuario.class);
        System.out.println(user.getNombre());
        amigos.add(user);
    }
    
    
    public ArrayList<Usuario> crearUsuarios(){
        
        usuarios= new ArrayList();
        ArrayList<Publicacion> misPublicaciones1= new ArrayList();
        
        Usuario usuario1= new Usuario();
        usuario1.setNombre("Carlitos Sanchez");
        usuario1.setTelefono("12345678");
        usuario1.setId(1);
        usuario1.setFoto("https://ichef.bbci.co.uk/news/640/amz/worldservice/live/assets/images/2010/05/16/100516091453_shrekgrande.jpg");
        // Publicaión 1 
        Publicacion publicacion1= new Publicacion();
        publicacion1.setTexto("Ola bb, vusco poyita, scrivem3");
        publicacion1.setUrl("https://www.generadormemes.com/media/created/1200x675xdrb3rzs56f0619vm3o7p0ez6sxnoblg3876u4h5yieyiemmymkw0zm1pp90fyyj.jpg.pagespeed.ic.meme.jpg");
        //Se agrega la publicación al arreglo
        misPublicaciones1.add(publicacion1);
        // Publicaión 2
        Publicacion publicacion1A= new Publicacion();
        publicacion1A.setTexto("Zoy el fe0 de lax n3naz lyndaz");
        publicacion1A.setUrl("https://www.generadormemes.com/media/created/1200x675xtrd8eggm3r15j3edi61aehlul2uunvaj5gft0j40n5q97cpw526xxvv844yk4uv.jpg.pagespeed.ic.meme.jpg");
        //Se agrega la publicación al arreglo
        misPublicaciones1.add(publicacion1A);
        // Publicaión 3
        Publicacion publicacion1B= new Publicacion();
        publicacion1B.setTexto("Piko pa laz ladis");
        publicacion1B.setUrl("https://pbs.twimg.com/profile_images/850736430963986432/S0ATyyiW.jpg");
        //Se agrega la publicación al arreglo
        misPublicaciones1.add(publicacion1B);
        //Se agrega el arreglo de publicaciones al usuario
        usuario1.setPublicaciones(misPublicaciones1);
        //Se agrega el usuario a la lista de usuarios 
        usuarios.add(usuario1);
        
        // ***********************************************************************************************
        
        ArrayList<Publicacion> misPublicaciones2= new ArrayList();
        Usuario usuario2= new Usuario();
        usuario2.setNombre("Débora Melo");
        usuario2.setTelefono("312567455345");
        usuario2.setId(2);
        usuario2.setFoto("https://st2.depositphotos.com/3763869/6593/v/600/depositphotos_65930119-stock-illustration-granny.jpghttps://i.pinimg.com/236x/22/0d/d1/220dd1892b9bcb957a3a5a245effc459--ice-age-funny-faces.jpg");
        
        Publicacion publicacion2= new Publicacion();
        publicacion2.setTexto("¿Para que quieres a otras si me tienes a mi?");
        publicacion2.setUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQDpxKhz17lTEG7Seq37rN8eNHnt9E2_axUVA&usqp=CAU");
        
        misPublicaciones2.add(publicacion2);
        
        Publicacion publicacion2A= new Publicacion();
        publicacion2A.setTexto("Para el amor no hay edad");
        publicacion2A.setUrl("https://chistesd.com/wp-content/uploads/JOVEN-SE-CASA-CON-ANCIANA.jpg");
        
        misPublicaciones2.add(publicacion2A);
        
        Publicacion publicacion2B= new Publicacion();
        publicacion2B.setTexto("Vamos a jugar al golozo, sube baja y yo te lo rozo");
        publicacion2B.setUrl("https://i.imgur.com/UkKmaVm.jpg");
        
        misPublicaciones2.add(publicacion2B);
        
        usuario2.setPublicaciones(misPublicaciones2);
        
        usuarios.add(usuario2);
        
        // ***********************************************************************************************
        ArrayList<Publicacion> misPublicaciones3= new ArrayList();
        Usuario usuario3= new Usuario();
        usuario3.setNombre("Benito Camelo");
        usuario3.setTelefono("345679786678");
        usuario3.setId(3);
        usuario3.setFoto("https://www.elcomercio.com/files/og_thumbnail/uploads/2019/05/23/5ce6c4f47a872.jpeg");
        
        Publicacion publicacion3= new Publicacion();
        publicacion3.setTexto("Hombre fiel, soltero, amo los niños, con cuerpo de 20, busco mujer que me haga feliz");
        publicacion3.setUrl("https://img.menzig.es/a/0000/685-h0.jpg");
        
        misPublicaciones3.add(publicacion3);
        
        Publicacion publicacion3A= new Publicacion();
        publicacion3A.setTexto("Me dijeron que esto es una red social para conseguir pareja, ¿Y las chicas?");
        publicacion3A.setUrl("https://www.generadormemes.com/media/templates/xabraham_simpson.jpg.pagespeed.ic.imagenes-plantillas-caras-crear-hacer-memes-memegenerator.jpg");
        
        misPublicaciones3.add(publicacion3A);
        
        Publicacion publicacion3B= new Publicacion();
        publicacion3B.setTexto("60 años de amistad y vamos por más");
        publicacion3B.setUrl("https://e00-elmundo.uecdn.es/assets/multimedia/imagenes/2018/07/25/15325243453772.png");
        
        misPublicaciones3.add(publicacion3B);
        
        usuario3.setPublicaciones(misPublicaciones3);
        
        usuarios.add(usuario3);
        
        // ***********************************************************************************************
        ArrayList<Publicacion> misPublicaciones4= new ArrayList();
        Usuario usuario4= new Usuario();
        usuario4.setNombre("Zoyla Vaca");
        usuario4.setTelefono("31149867494838");
        usuario4.setId(4);
        usuario4.setFoto("https://ih1.redbubble.net/image.474256936.5239/flat,800x800,075,f.jpg");
        
        Publicacion publicacion4= new Publicacion();
        publicacion4.setTexto("A bueno, te me cuidas");
        publicacion4.setUrl("https://i.pinimg.com/236x/cb/1f/d5/cb1fd57bae58116c8327cbf4fef3d4e9.jpg");
        
        misPublicaciones4.add(publicacion4);
        
        Publicacion publicacion4A= new Publicacion();
        publicacion4A.setTexto("Manos arriba, calzones abajo, si no se los baja, yo se los bajo");
        publicacion4A.setUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTWndBf7Ny7R3D-mfRMNOWAc1PYDz_vCBrqtw&usqp=CAU");
        
        misPublicaciones4.add(publicacion4A);
        
        Publicacion publicacion4B= new Publicacion();
        publicacion4B.setTexto("¿POR QUE ESTA CAMINANDO DESCALZO?");
        publicacion4B.setUrl("https://thumbs.dreamstime.com/b/vieja-mujer-enojada-que-amenaza-con-un-rodillo-38854147.jpg");
        
        misPublicaciones4.add(publicacion4B);
        
        usuario4.setPublicaciones(misPublicaciones4);
        
        usuarios.add(usuario4);
        
        // ***********************************************************************************************
        ArrayList<Publicacion> misPublicaciones5= new ArrayList();
        Usuario usuario5= new Usuario();
        usuario5.setNombre("Alan Brito Delgado");
        usuario5.setTelefono("314769485749484");
        usuario5.setId(5);
        usuario5.setFoto("https://i.pinimg.com/236x/33/bf/33/33bf33f1ef7e02adae55742b8f851000--gifs.jpg");
        
        Publicacion publicacion5= new Publicacion();
        publicacion5.setTexto("Mejor no hubieras nacido si ibas a tener esa cara");
        publicacion5.setUrl("https://i.pinimg.com/564x/c6/dc/fc/c6dcfcdad8401eabea7cb537671a629c.jpg");
        
        misPublicaciones5.add(publicacion5);
        
        Publicacion publicacion5A= new Publicacion();
        publicacion5A.setTexto("Gracias por ser la decepción de la familia");
        publicacion5A.setUrl("https://www.excelsior.com.mx/media//inside-the-note/pictures/2019/08/13/jmg.jpg");
        
        misPublicaciones5.add(publicacion5A);
        
        Publicacion publicacion5B= new Publicacion();
        publicacion5B.setTexto("Personas mediocres y tú");
        publicacion5B.setUrl("https://img3.codigonuevo.com/0c/e1/6a/abuelo-meme-930x600.jpg");
        
        misPublicaciones5.add(publicacion5B);
        
        usuario5.setPublicaciones(misPublicaciones5);
        
        usuarios.add(usuario5);
        
        // ***********************************************************************************************
        ArrayList<Publicacion> misPublicaciones6= new ArrayList();
        Usuario usuario6= new Usuario();
        usuario6.setNombre("Susana Oria");
        usuario6.setTelefono("31597438739");
        usuario6.setId(6);
        usuario6.setFoto("https://static.vix.com/es/sites/default/files/styles/1x1/public/a/abuelita-viendo-su-computadora-meme.jpg");
        
        Publicacion publicacion6= new Publicacion();
        publicacion6.setTexto("Piolin para todos");
        publicacion6.setUrl("https://em.wattpad.com/4dd58eb7f1d46ab1560edd4ea056104c7ca4953e/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f5976777261486a4a364e517a38413d3d2d3830373534343832342e313564393066643137393063663332333639383231373231313533332e6a7067?s=fit&w=720&h=720");
        
        misPublicaciones6.add(publicacion6);
        
        Publicacion publicacion6A= new Publicacion();
        publicacion6A.setTexto("Gracias amigos!");
        publicacion6A.setUrl("https://em.wattpad.com/5f054b885f6561a4dbf769d60a5ea6f51863418a/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f3946715170733741365859542d413d3d2d3739383236323135382e313564306636333439636166653137383333363635363336363231372e6a7067?s=fit&w=720&h=720");
        
        misPublicaciones6.add(publicacion6A);
        
        Publicacion publicacion6B= new Publicacion();
        publicacion6B.setTexto("Busco muchacho para mantener como su sugar mommy");
        publicacion6B.setUrl("https://i.pinimg.com/564x/13/91/16/13911661a2b380683380c69de161b879.jpg");
        
        misPublicaciones6.add(publicacion6B);
        
        usuario6.setPublicaciones(misPublicaciones6);
        
        usuarios.add(usuario6);
        
        // ***********************************************************************************************
        ArrayList<Publicacion> misPublicaciones7= new ArrayList();
        Usuario usuario7= new Usuario();
        usuario7.setNombre("Aquiles Brinco");
        usuario7.setTelefono("320438935938");
        usuario7.setId(7);
        usuario7.setFoto("https://media.metrolatam.com/2020/04/13/screenshot202004-61a21d1c3b7c6ecddf8c9b508bb28134-1200x800.jpg");
        
        Publicacion publicacion7= new Publicacion();
        publicacion7.setTexto("Recuerda cuidarte bro ");
        publicacion7.setUrl("https://image.freepik.com/foto-gratis/cara-hombre-hipster-barbudo-maduro-que-piensa-mascara-protegerse-brote-virus-corona-dentro-tren_251136-25883.jpg");
        
        misPublicaciones7.add(publicacion7);
        
        Publicacion publicacion7A= new Publicacion();
        publicacion7A.setTexto("¿Yo seré su Sugar Daddy?");
        publicacion7A.setUrl("https://i.ytimg.com/vi/BJAwnUAmycY/maxresdefault.jpg");
        
        misPublicaciones7.add(publicacion7A);
        
        Publicacion publicacion7B= new Publicacion();
        publicacion7B.setTexto("¿Algún interesado?");
        publicacion7B.setUrl("https://rlv.zcache.es/invitacion_para_hombre_de_la_fiesta_de_cumpleanos-re135906da8a048c4b20b3cf6705e8b1a_zk9ma_540.jpg?rlvnet=1");
        
        misPublicaciones7.add(publicacion7B);
        
        usuario7.setPublicaciones(misPublicaciones7);
        
        usuarios.add(usuario7);
        
        
        return usuarios;
    }
    
    public static void main(String[] args) {
        Modelo m = new Modelo();
        ArrayList<Usuario> z = new ArrayList();
        m.crearUsuarios();
        
        try {
            m.escribirFile();
            z = m.leerFile();
        } catch (IOException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(z.get(0).getNombre());
        System.out.println(z.get(1).getNombre());
            System.out.println(z.get(2).getNombre());
            System.out.println(z.get(3).getNombre());
            System.out.println(z.get(4).getNombre());
            System.out.println(z.get(5).getNombre());
    }
    
    
}
