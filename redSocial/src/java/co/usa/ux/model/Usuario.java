/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usa.ux.model;

import java.io.Serializable;
import java.util.ArrayList;



/**
 *
 * @author LENOVO
 */
public class Usuario implements Serializable {
    private String nombre;
    private String telefono;
    private int id;
    private String foto;
    private ArrayList<Publicacion> publicaciones;
    private ArrayList<Usuario> amigos;

    public Usuario(String nombre, String telefono, int id, String foto,ArrayList<Publicacion> publicaciones, ArrayList<Usuario> amigos) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.id = id;
        this.foto=foto;
        this.publicaciones=publicaciones;
        this.amigos=amigos;
    }
    
        public Usuario() {
        this.nombre = "";
        this.telefono = "";
        this.id = 0;
        this.foto="";
        this.publicaciones=null;
        this.amigos=null;
        }

    public ArrayList<Usuario> getAmigos() {
        return amigos;
    }

    public void setAmigos(ArrayList<Usuario> amigos) {
        this.amigos = amigos;
    }

    public ArrayList<Publicacion> getPublicaciones() {
        return publicaciones;
    }

    public void setPublicaciones(ArrayList<Publicacion> publicaciones) {
        this.publicaciones = publicaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
}
